from django.shortcuts import render
from articles.models import Article
from .forms import ArticleForm, CommentForm


def index(request):
    if request.method == "POST":
        form = ArticleForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
    else:
        form = ArticleForm()
    # articles = Article.objects.all()
    articles = Article.objects.filter(is_active=True)
    context = {
        'articles': articles,
        'form': form
    }
    return render(request, 'index.html', context)

def retrieve(request, id):
    article = Article.objects.get(id=id)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            form.save()
    comment_form = CommentForm()
    context = {
        'article': article,
        'comment_form': comment_form
    }
    return render(request, 'retrieve.html', context)
