from django.contrib import admin

from articles.models import Article, Comment, ArticleImage


class ArticleAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'is_active']

class CommentAdmin(admin.ModelAdmin):
    list_display = ['id', 'content', 'related_article', 'related_user']

class ArticleImageAdmin(admin.ModelAdmin):
    list_display = ['id', 'image', 'related_article', 'related_user']

admin.site.register(Article, ArticleAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(ArticleImage, ArticleImageAdmin)